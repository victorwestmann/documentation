# Translation of Inkscape's interpolate tutorial to Ukrainian
#
#
# Nazarii Ritter <nazariy.ritter@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-12-17 00:51+0100\n"
"PO-Revision-Date: 2017-07-17 10:19+0300\n"
"Last-Translator: Nazarii Ritter <nazariy.ritter@gmail.com>, 2018\n"
"Language-Team: \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#: interpolate-f20.svg:52(format) interpolate-f19.svg:49(format)
#: interpolate-f18.svg:49(format) interpolate-f17.svg:49(format)
#: interpolate-f16.svg:49(format) interpolate-f15.svg:49(format)
#: interpolate-f14.svg:49(format) interpolate-f13.svg:49(format)
#: interpolate-f12.svg:49(format) interpolate-f11.svg:49(format)
#: interpolate-f10.svg:49(format) interpolate-f09.svg:49(format)
#: interpolate-f08.svg:49(format) interpolate-f07.svg:49(format)
#: interpolate-f05.svg:49(format) interpolate-f04.svg:49(format)
#: interpolate-f03.svg:49(format) interpolate-f02.svg:49(format)
#: interpolate-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: interpolate-f11.svg:117(tspan) interpolate-f04.svg:117(tspan)
#: interpolate-f02.svg:126(tspan)
#, no-wrap
msgid "Exponent: 0.0"
msgstr "Експонента: 0,0"

#: interpolate-f11.svg:121(tspan) interpolate-f04.svg:121(tspan)
#: interpolate-f02.svg:130(tspan)
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "Кроків інтерполяції: 6"

#: interpolate-f11.svg:125(tspan) interpolate-f04.svg:125(tspan)
#: interpolate-f02.svg:134(tspan)
#, no-wrap
msgid "Interpolation Method: 2"
msgstr "Метод інтерполяції: 2"

#: interpolate-f11.svg:129(tspan) interpolate-f04.svg:129(tspan)
#: interpolate-f02.svg:138(tspan)
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr "Дублювати кінцеві контури: вимкнено"

#: interpolate-f11.svg:133(tspan) interpolate-f04.svg:133(tspan)
#: interpolate-f02.svg:142(tspan)
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr "Інтерполяція стилю: вимкнено"

#: tutorial-interpolate.xml:6(firstname)
msgid "Ryan"
msgstr ""

#: tutorial-interpolate.xml:6(surname)
msgid "Lerch"
msgstr ""

#: tutorial-interpolate.xml:6(email)
#, fuzzy
msgid "ryanlerch at gmail dot com"
msgstr "Ryan Lerch, ryanlerch at gmail dot com"

#: tutorial-interpolate.xml:10(title)
msgid "Interpolate"
msgstr "Інтерполяція"

#: tutorial-interpolate.xml:13(para)
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""
"Цей документ пояснює як користуватися додатком «Inkscape» «Інтерполяція»."

#: tutorial-interpolate.xml:18(title)
msgid "Introduction"
msgstr "Вступ"

#: tutorial-interpolate.xml:19(para)
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""
"«Інтерполяція» створює <firstterm>лінійну інтерполяцію</firstterm> між двома "
"чи більше виділеними контурами. По суті, це означає, що він «заповнює "
"щілини» між контурами і перетворює їх відповідно до кількості заданих кроків."

#: tutorial-interpolate.xml:20(para)
#, fuzzy
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate</"
"guimenuitem></menuchoice> from the menu."
msgstr ""
"Для використання додатку «Інтерполяція», виділіть контури, які потрібно "
"трансформувати та виберіть в меню <command>Додатки &gt; Використання "
"контуру &gt; Інтерполяція</command>."

#: tutorial-interpolate.xml:21(para)
#, fuzzy
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""
"Перед викликом додатку, об'єкти, які потрібно трансформувати, мають бути "
"<emphasis>контурами</emphasis>. Це робиться за допомогою виділення об'єкту і "
"використання <command>Контур &gt; Об'єкт у контур</command> або <keycap>Shift"
"+Ctrl+C</keycap>. Якщо об'єкти не є контурами, то додаток нічого не створить."

#: tutorial-interpolate.xml:25(title)
msgid "Interpolation between two identical paths"
msgstr "Інтерполяція між двома ідентичними контурами"

#: tutorial-interpolate.xml:26(para)
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"Найпростіше використання додатку «Інтерполяція» – інтерполяція між двома "
"ідентичними контурами. При виклику додатку –  результатом є те, що простір "
"між контурами заповнений дублікатами вихідних контурів. Кількість кроків "
"визначає кількість дублікатів, що розміщуються."

#: tutorial-interpolate.xml:27(para) tutorial-interpolate.xml:51(para)
msgid "For example, take the following two paths:"
msgstr "Для прикладу розглянемо наступні два контури:"

#: tutorial-interpolate.xml:36(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""
"Тепер виділіть ці два контури і запустіть додаток «Інтерполяція» з "
"налаштуваннями показаними на наступному малюнку."

#: tutorial-interpolate.xml:45(para)
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""
"Як видно з результату зверху, простір між двома колоподібними контурами було "
"заповнено 6‑ма (кількість кроків інтерполяції) іншими колоподібними "
"контурами. Також зверніть увагу, що додаток згруповує ці фігури разом."

#: tutorial-interpolate.xml:49(title)
msgid "Interpolation between two different paths"
msgstr "Інтерполяція між двома різними контурами"

#: tutorial-interpolate.xml:50(para)
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""
"При інтерполяції між двома різними контурами, програма інтерполює форму від "
"одного контуру до іншого. Результатом є послідовність трансформацій між "
"контурами з неперервністю, що задається значенням «Кроки інтерполяції»."

#: tutorial-interpolate.xml:60(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""
"Тепер виділіть обидва контури і запустіть додаток «Інтерполяція». Результат "
"має бути як цей:"

#: tutorial-interpolate.xml:69(para)
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"Як видно з результату вище, простір між колоподібним контуром і "
"трикутникоподібним заповнений 6‑ма контурами, які змінюють форму від одного "
"контуру до іншого."

#: tutorial-interpolate.xml:71(para)
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""
"При використанні додатку «Інтерполяція» з двома різними контурами, положення "
"початкового вузла кожного з контурів – важливе. Щоб знайти початковий вузол "
"контуру, виділіть контур, потім активуйте інструмент «Вузли», щоб "
"відобразити вузли, і натисніть «<keycap>TAB</keycap>». Перший виділений "
"вузол і є початковим вузлом контуру."

#: tutorial-interpolate.xml:73(para)
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"Див. малюнок нижче, який ідентичний попередньому прикладу, крім того, що на "
"ньому відображені вузлові точки. Зелені вузли кожного контуру і є "
"початковими вузлами."

#: tutorial-interpolate.xml:82(para)
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""
"Попередній приклад (знову показаний нижче) було зроблено з цими вузлами як "
"початковими."

#: tutorial-interpolate.xml:91(para)
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr ""
"Тепер зверніть увагу на зміни в результаті інтерполяції при віддзеркаленні "
"трикутного контуру так, що початковий вузол – в іншому місці:"

#: tutorial-interpolate.xml:110(title)
msgid "Interpolation Method"
msgstr "Метод інтерполяції"

#: tutorial-interpolate.xml:111(para)
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either Interpolation Method 1 or 2."
msgstr ""
"Одним з параметрів додатку «Інтерполяція» є «Метод інтерполяції». В ньому "
"впроваджено 2 методи інтерполяції і вони відрізняються способом розрахунку "
"кривих нових фігур. Вибір або метод інтерполяції 1 або 2."

#: tutorial-interpolate.xml:113(para)
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr ""
"В прикладах вище, було використано метод інтерполяції 2, і результат був "
"таким:"

#: tutorial-interpolate.xml:122(para)
msgid "Now compare this to Interpolation Method 1:"
msgstr "Тепер порівняйте його з методом інтерполяції 1:"

#: tutorial-interpolate.xml:131(para)
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"Відміннісь між методами обрахунку чисел не входить в рамки цього документу, "
"тому просто спробуйте обидва методи, і використовуйте той, який дає "
"результат, найближчий до потрібного."

#: tutorial-interpolate.xml:136(title)
msgid "Exponent"
msgstr "Експонента"

#: tutorial-interpolate.xml:138(para)
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""
"<firstterm>Параметр «Експонента»</firstterm> регулює відстань між кроками "
"інтерполяції. Експонента, рівна 0, робить однаковими відстані між копіями."

#: tutorial-interpolate.xml:140(para)
msgid "Here is the result of another basic example with an exponent of 0."
msgstr "Ось – результат іншого базового прикладу з експонентою рівною 0."

#: tutorial-interpolate.xml:149(para)
msgid "The same example with an exponent of 1:"
msgstr "Той самий результат з експонентою рівною 1:"

#: tutorial-interpolate.xml:158(para)
msgid "with an exponent of 2:"
msgstr "з експонентою рівною 2:"

#: tutorial-interpolate.xml:167(para)
msgid "and with an exponent of -1:"
msgstr "та з експонентою рівною –1:"

#: tutorial-interpolate.xml:176(para)
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"Працюючи з експонентами в додатку «Інтерполяція», <emphasis>порядок</"
"emphasis> вибору об'єктів – важливий. У прикладах вище, зіркоподібний контур "
"зліва було вибрано першим, а шестикутникоподібний контур справа – другим."

#: tutorial-interpolate.xml:178(para)
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr ""
"Погляньте на результат, коли контур справа було вибрано першим. Значення "
"експоненти було встановлено рівним 1:"

#: tutorial-interpolate.xml:189(title)
msgid "Duplicate Endpaths"
msgstr "Дублювати кінцеві контури"

#: tutorial-interpolate.xml:190(para)
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"Цей параметр визначає чи крупа контурів, згенерованих додатком, "
"<emphasis>включає копію</emphasis> початкових контурів, для яких "
"застосовувалась інтерполяція."

#: tutorial-interpolate.xml:194(title)
msgid "Interpolate Style"
msgstr "Інтерполяція стилю"

#: tutorial-interpolate.xml:195(para)
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"Цей параметр – одна з витончених функцій додатку «Інтерполяція». вона змушує "
"додаток спробувати змішати стиль контурів на кожному кроці. Тому, якщо "
"початковий та кінцевий контури різного кольору, то контури, що генеруються "
"будуть також змінюватися покроково."

#: tutorial-interpolate.xml:197(para)
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""
"Ось – приклад, в якому функція «Інтерполяція стилю» застосовується для "
"заповнення контуру:"

#: tutorial-interpolate.xml:207(para)
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "«Інтерполяція стилю» також впливає на штрих контуру:"

#: tutorial-interpolate.xml:216(para)
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr ""
"Звичайно ж, що контури початкової та кінцевої точок також не мають бути "
"однаковими:"

#: tutorial-interpolate.xml:227(title)
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr "Використання «Інтерполяції» для імітації градієнтів неправильної форми"

#: tutorial-interpolate.xml:229(para)
#, fuzzy
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""
"В «Inkscape» неможливе (поки що), створення градієнтів окрім як лінійних "
"(прямолінійних) чи радіальних (круглі). Однак, їх можна зімітувати, "
"використовуючи додаток «Інтерполяція» та «Інтерполяція стилю». В наступному "
"простому прикладі намалюйте дві лінії з різними штрихами:"

#: tutorial-interpolate.xml:237(para)
msgid "And interpolate between the two lines to create your gradient:"
msgstr "Та інтерполюйте між двома лініями, щоб створити градієнт:"

#: tutorial-interpolate.xml:249(title)
msgid "Conclusion"
msgstr "Заключення"

#: tutorial-interpolate.xml:250(para)
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""
"Як показувалось вище, додаток «Інтерполяція» «Inkscape» – потужний "
"інструмент. Цей урок охоплює основи цього додатку, але експериментування – "
"ключ до подальшого відкриття інтерполяції."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-interpolate.xml:0(None)
msgid "translator-credits"
msgstr "Nazarii Ritter <nazariy.ritter@gmail.com>, 2017"
