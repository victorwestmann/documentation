# Translators:
# Somogyvári Róbert <cr04ch at gmail>, 2009.
# Arpad Biro <biro.arpad gmail>, 2009.  
# Balázs Meskó <meskobalazs@gmail.com>, 2018
# Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tips and Tricks\n"
"POT-Creation-Date: 2019-12-17 00:51+0100\n"
"PO-Revision-Date: 2019-11-23 13:40+0100\n"
"Last-Translator: Gyuris Gellért <bubu@ujevangelizacio.hu>, 2019\n"
"Language-Team: Hungarian (https://www.transifex.com/fsf-hu/teams/77907/hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: tips-f06.svg:136(format) tips-f05.svg:558(format) tips-f04.svg:70(format)
#: tips-f03.svg:49(format) tips-f02.svg:49(format) tips-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tutorial-tips.xml:6(title)
msgid "Tips and Tricks"
msgstr "Tippek"

#: tutorial-tips.xml:9(para)
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"Az alábbi tippek és trükkök segítenek elsajátítani az Inkscape használatát, "
"és bemutatunk néhány munkát gyorsító „rejtett” tulajdonságot is."

#: tutorial-tips.xml:17(title)
msgid "Radial placement with Tiled Clones"
msgstr "Sugárirányú elhelyezés csempézett klónokkal"

#: tutorial-tips.xml:18(para)
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""
"Az könnyen átlátható, hogy a  <guimenuitem>csempézett klónok létrehozása</"
"guimenuitem> párbeszédablakkal hogyan készíthető derékszögű rács vagy minta. "
"De mi a teendő akkor, ha <firstterm>sugárirányú</firstterm> elrendezésre van "
"szükség, ahol az objektumoknak közös a forgáspontjuk? Ez szintén megoldható!"

#: tutorial-tips.xml:23(para)
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"Ha a sugárirányú minta csak  3, 4, 6, 8, vagy 12 elemű kell legyen, akkor "
"érdemes a P3, P31M, P3M1, P4, P4M, P6, vagy a P6M szimmetriákat kipróbálni. "
"Ezek jól fognak működni a hópelyhek és hasonlók esetében. Egy általánosabb "
"módszer azonban a következő."

#: tutorial-tips.xml:28(para)
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""
"Válassza a P1 szimmetriát (egyszerű eltolás), majd az <guimenuitem>Eltolás</"
"guimenuitem> lapon <emphasis>ellensúlyozza</emphasis> ezt az eltolást úgy, "
"hogy a <guilabel>Soronként / Y irányú eltolás</guilabel> és az "
"<guilabel>Oszloponként / X irányú eltolás</guilabel> értékét is −100%-ra "
"állítja. Így minden klón pontosan az eredeti objektumra halmozódik. Már csak "
"annyit kell tennie, hogy a <guimenuitem>Forgatás</guimenuitem> lapon megad "
"némi oszloponkénti elforgatást, és a mintát egy sorból és több oszlopból "
"készíti. Az alábbi példa egy függőleges vonalból készült 30 oszloppal, "
"minden oszlop 6°-os elforgatásával:"

#: tutorial-tips.xml:45(para)
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"clones, unlink them first)."
msgstr ""
"Ebből már könnyen kaphat egy óralapot, ha a középső részt kivágja vagy "
"egyszerűen letakarja egy fehér körrel (kapcsolja le a klónt, ha logikai "
"műveletet akar végezni rajta)."

#: tutorial-tips.xml:49(para)
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""
"Még érdekesebb hatás érhető el sorokat és oszlopokat is használva. Az alábbi "
"minta 10 oszloppal és 8 sorral készült, soronként 2°-os, oszloponként 18°-os "
"forgatással. Itt minden vonalcsoport egy „oszlop”, így a csoportok 18°-"
"onként követik egymást; az egyedi vonalak minden oszlopban 2°-onként "
"helyezkednek el:"

#: tutorial-tips.xml:63(para)
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""
"A fenti példában a vonalak a középpontjuk körül forogtak. De hogyan oldható "
"meg a középpont alakzaton kívül helyezése? Kattintson kétszer a kijelölés "
"eszközzel az objektumon, hogy a forgatási módot aktiválja. Most mozgassa el "
"az objektum forgatási középpontját (egy kis kereszt alakú fogantyú) arra a "
"pontra, ahol szeretné, hogy a csempézett klónok forgatási középpontja "
"legyen. Alkalmazza a <guimenuitem>Csempézett klón létrehozása</guimenuitem> "
"parancsot az objektumon. Így készíthet – véletlenszerűséget is hozzáadva a "
"méretezéshez, a forgatáshoz és esetleg az átlátszósághoz – tetszetős "
"„kitöréseket” vagy „csillagrobbanásokat”:"

#: tutorial-tips.xml:81(title)
msgid "How to do slicing (multiple rectangular export areas)?"
msgstr ""
"Hogyan lehet szeletelést (téglalap alakú exportterületeket) létrehozni?"

#: tutorial-tips.xml:82(para)
msgid ""
"Create a new layer, in that layer create invisible rectangles covering parts "
"of your image. Make sure your document uses the px unit (default), turn on "
"grid and snap the rects to the grid so that each one spans a whole number of "
"px units. Assign meaningful ids to the rects, and export each one to its own "
"file (<menuchoice><guimenu>File</guimenu><guimenuitem>Export PNG Image</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</keycap></"
"keycombo>)). Then the rects will remember their export filenames. After "
"that, it's very easy to re-export some of the rects: switch to the export "
"layer, use <keycap>Tab</keycap> to select the one you need (or use Find by "
"id), and click <guibutton>Export</guibutton> in the dialog. Or, you can "
"write a shell script or batch file to export all of your areas, with a "
"command like:"
msgstr ""
"Hozzon létre egy új réteget, majd ezen a rétegen láthatatlan téglalapokat, "
"melyek befedik a kép részleteit. A dokumentum beállított mértékegysége a px "
"(ez az alapértelmezett) legyen. Kapcsolja be a rácsot, majd igazítsa rácshoz "
"a téglalapokat, ezáltal mindegyik mérete egész számú px lesz. Minden "
"téglalapra állítson be valamilyen értelmes azonosítót, és exportálja őket "
"egy-egy külön fájlba (<menuchoice><guimenu>Fájl</guimenu><guimenuitem>PNG "
"kép exportálása…</guimenuitem></menuchoice> (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</"
"keycap></keycombo>)). Így minden téglalap megjegyzi a hozzá tartozó "
"fájlnevet. Ezután már nagyon könnyű lesz újra exportálni a téglalapokat: "
"váltson az exportrétegre, a <keycap>Tab</keycap> billentyűvel (vagy a "
"Keresés funkcióval azonosító alapján) jelölje ki az exportálandó téglalapot, "
"majd a párbeszédablakban kattintson az <guibutton>Exportálásra</guibutton>. "
"De írhat egy parancsfájlt is, minden exportálandó területhez egy ilyen "
"paranccsal:"

#: tutorial-tips.xml:94(command)
msgid "inkscape -i area-id -t filename.svg"
msgstr "inkscape -i area-id -t fájlnév.svg"

#: tutorial-tips.xml:96(para)
msgid ""
"for each exported area. The <command>-t</command> switch tells it to use the "
"remembered filename hint, otherwise you can provide the export filename with "
"the <command>-e</command> switch. Alternatively, you can use the "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Web</"
"guisubmenu><guimenuitem>Slicer</guimenuitem></menuchoice> extensions, or "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Export</"
"guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice> for similar "
"results."
msgstr ""
"A <command>-t</command> kapcsoló jelzi a programnak, hogy a mentett "
"fájlneveket használja, ellenkező esetben az <command>-e</command> "
"kapcsolóval lehetne megadni az exportálandó fájl nevét. A másik lehetőség az "
"<menuchoice><guimenu>Kiterjesztések </guimenu><guisubmenu>Web</"
"guisubmenu><guimenuitem>Szeletelő</guimenuitem></menuchoice> kiterjesztés, "
"vagy a <menuchoice><guimenu>Kiterjesztések</guimenu><guisubmenu>Exportálás</"
"guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice>hasonló "
"eredménnyel."

#: tutorial-tips.xml:104(title)
msgid "Non-linear gradients"
msgstr "Nemlineáris színátmenetek"

#: tutorial-tips.xml:105(para)
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"Az SVG 1.1-es verziója nem támogatja a nemlineáris színátmeneteket (vagyis "
"ahol nemlineáris átmenet van egyik színből a másikba). Azonban mégis "
"készíthető hasonló, <firstterm>többfázisú</firstterm> színátmenettel."

#: tutorial-tips.xml:110(para)
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""
"Először készítsen egy egyszerű, kétfázisú színátmenetet (a kitöltés és "
"körvonal párbeszédablakban lehet ezt hozzárendelni vagy a színátmenet "
"eszközzel). Most adjon a színátmenet közepére egy új fázist a színátmenet "
"eszközzel vagy a színátmenet sávon való dupla kattintással vagy a négyzet "
"alakú színátmenet-fázis kijelölésével és az <guimenuitem>Új fázis beszúrása</"
"guimenuitem> gombra való kattintással a színátmenet eszköz fenn levő "
"eszköztárán. Kicsit húzza el az új fázist. Végül e középső fázis elé és mögé "
"is vegyen fel további fázisokat, melyeket szintén mozgasson el kissé, hogy "
"az átmenet egyenletes legyen. Minél több fázispont van, annál folyamatosabb "
"lesz az átmenet. Ez a kiinduló fekete-fehér, kétfázisú színátmenet:"

#: tutorial-tips.xml:127(para)
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"Itt pedig néhány „nemlineáris”, többfázisú színátmenetet láthat (vizsgálja "
"meg őket a Színátmenet-szerkesztővel):"

#: tutorial-tips.xml:141(title)
msgid "Excentric radial gradients"
msgstr "Excentrikus sugárirányú színátmenetek"

#: tutorial-tips.xml:142(para)
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap function=\"shift"
"\">Shift</keycap>. This will move the x-shaped <firstterm>focus handle</"
"firstterm> of the gradient away from its center. When you don't need it, you "
"can snap the focus back by dragging it close to the center."
msgstr ""
"A sugárirányú színátmenet nem csak szimmetrikus lehet. A Színátmenet "
"eszközzel a <keycap function=\"shift\">Shift</keycap> nyomva tartása mellett "
"húzza el a középső fogantyút. Így tudja elmozgatni a középpontból az átmenet "
"X alakú <firstterm>fókuszfogantyú</firstterm>ját. Ezt a vezérlőelemet "
"bármikor visszaigazíthatja középre, elég ha a színátmenet középpontjának "
"közelébe húzza."

#: tutorial-tips.xml:158(title)
msgid "Aligning to the center of the page"
msgstr "Igazítás a lap közepére"

#: tutorial-tips.xml:159(para)
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""
"Bármit a lap közepére vagy szélére igazíthat, ha kijelöli az objektumot vagy "
"csoportot, majd az <guimenuitem>Igazítás és elrendezés</guimenuitem> "
"párbeszédablakban (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>) az <guilabel>Ehhez viszonyítva</guilabel>: listából a "
"<guimenuitem>Lap</guimenuitem>ot választja."

#: tutorial-tips.xml:167(title)
msgid "Cleaning up the document"
msgstr "A dokumentum tisztítása"

#: tutorial-tips.xml:168(para)
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""
"Minden már nem használt színátmenet, minta és jelölő (pontosabban azok, "
"amelyeket Ön szerkesztett) megőrződik a megfelelő palettákon, és új "
"objektumokhoz újfent használható. De ha szeretné a dokumentumot "
"optimalizálni, akkor a <guimenu>Fájl</guimenu> menüben található "
"<guimenuitem>Dokumentum tisztítása</guimenuitem> segít ebben. Ez a parancs "
"minden használaton kívüli színátmenetet, mintát és jelölőt eltávolít a "
"dokumentumból, csökkentve ezzel a fájl méretét."

#: tutorial-tips.xml:177(title)
msgid "Hidden features and the XML editor"
msgstr "Rejtett sajátságok és az XML-szerkesztő"

#: tutorial-tips.xml:178(para)
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""
"Az XML-szerkesztő (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>X</keycap></"
"keycombo>) külső szerkesztő igénybevétele nélkül is lehetőséget ad majdnem "
"mindent módosítani a dokumentumban. Ezenfelül az Inkscape általában több SVG-"
"tulajdonságot támogat, mint amennyihez a grafikus felhasználói felületen "
"hozzáférést biztosít. Az XML-szerkesztő az egyik út az efféle "
"tulajdonságokhoz (ha Ön tisztában van az SVG-vel)."

#: tutorial-tips.xml:187(title)
msgid "Changing the rulers' unit of measure"
msgstr "A vonalzók mértékegységének módosítása"

#: tutorial-tips.xml:188(para)
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-left corner and "
"preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Page</"
"guimenuitem> tab."
msgstr ""
"Az alapértelmezett sablonban a vonalzók mértékegysége a mm. A koordináták is "
"ebben a mértékegységben jelennek meg a bal alsó sarokban, és a mértékegység-"
"választó listákban is ez az előre kiválasztott. (Az <mousebutton>egeret</"
"mousebutton> a vonalzó felett tartva a gyorstippen mindig láthatja, hogy "
"éppen mi a mértékegység.) A <guimenuitem>Dokumentum-beállítások</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) "
"<guimenuitem>Lap</guimenuitem> részén állítható be az "
"<guimenuitem>Alapértelmezett mértékegység</guimenuitem>."

#: tutorial-tips.xml:199(title)
msgid "Stamping"
msgstr "Bélyegző"

#: tutorial-tips.xml:200(para)
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""
"Egy objektumról gyorsan készíthet sok másolatot a <firstterm>bélyegző</"
"firstterm> funkcióval. Mialatt egy objektumot mozgat (vagy méretez vagy "
"forgat), az <mousebutton role=\"click\">egérgomb</mousebutton> nyomva "
"tartása közben nyomja le a <keycap>Szóköz</keycap> billentyűt. Ez egy "
"„lenyomatot” hagy az adott objektumról. A műveletet ismételheti, ahányszor "
"csak tetszik."

#: tutorial-tips.xml:209(title)
msgid "Pen tool tricks"
msgstr "A Toll eszköz fortélyai"

#: tutorial-tips.xml:210(para)
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr "A Toll (Bézier) eszközzel befejezheti az aktuális vonalat:"

#: tutorial-tips.xml:214(para)
msgid "Press <keycap>Enter</keycap>"
msgstr "az <keycap>Enter</keycap> megnyomásával;"

#: tutorial-tips.xml:217(para)
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr ""
"a bal egérgombbal <mousebutton role=\"double-click\">duplán kattintva</"
"mousebutton>;"

#: tutorial-tips.xml:220(para)
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""
"a <mousebutton role=\"right-click\">jobb</mousebutton> egérgombbal kattintva;"

#: tutorial-tips.xml:223(para)
msgid "Select another tool"
msgstr "egy másik eszköz kiválasztásával."

#: tutorial-tips.xml:228(para)
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""
"Amíg egy útvonal befejezetlen (azaz zöld, az aktuális szakasz pedig vörös), "
"addig objektumként nem létezik a dokumentumban. Ezért ilyenkor a "
"<guimenuitem>Visszavonás</guimenuitem> helyett az <keycap>Esc</keycap> (az "
"egész útvonal törlése) vagy a <keycap>Backspace</keycap> (az utolsó szakasz "
"törlése) billentyűvel lehet érvényteleníteni az útvonalat."

#: tutorial-tips.xml:235(para)
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""
"Egy létező útvonalhoz új al-útvonal adható az útvonal kijelölése után, a "
"<keycap function=\"shift\">Shift</keycap> nyomva tartása mellett tetszőleges "
"helyen kezdve a rajzolást. De ha egyszerűen csak <emphasis>folytatni</"
"emphasis> szeretne egy útvonalat, akkor a Shift nem is kell; elég a kijelölt "
"útvonal egyik horgonyától kezdenie rajzolni."

#: tutorial-tips.xml:244(title)
msgid "Entering Unicode values"
msgstr "Unicode-értékek bevitele"

#: tutorial-tips.xml:245(para)
msgid ""
"While in the Text tool, pressing <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles between Unicode and "
"normal mode. In Unicode mode, each group of 4 hexadecimal digits you type "
"becomes a single Unicode character, thus allowing you to enter arbitrary "
"symbols (as long as you know their Unicode codepoints and the font supports "
"them). To finish the Unicode input, press <keycap>Enter</keycap>. For "
"example, <keycombo action=\"seq\"><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</"
"keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo> inserts an em-"
"dash (—). To quit the Unicode mode without inserting anything press "
"<keycap>Esc</keycap>."
msgstr ""
"A Szöveg eszközzel dolgozva a <keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap></keycombo> lenyomásával válthat a Unicode és a "
"szokásos beviteli mód között. Unicode módban minden karaktert egy 4 jegyű "
"hexadecimális szám megadásával tud beírni, így lehetőség van tetszőleges "
"jelek bevitelére (feltéve, hogy Ön ismeri az adott karakter Unicode-értékét, "
"és az aktuális betűkészlet tartalmazza is a karaktert). A Unicode módot az "
"<keycap>Enter</keycap> billentyűvel hagyhatja el. Például a <keycombo action="
"\"seq\"><keycap function=\"control\">Ctrl</keycap><keycap>U</"
"keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</keycap><keycap>4</"
"keycap><keycap>Enter</keycap></keycombo> beszúrja az em-dash (—) karaktert. "
"Ha beszúrás nélkül szeretne kilépni a Unicode módból, akkor nyomja le az "
"<keycap>Esc</keycap> billentyűt."

#: tutorial-tips.xml:254(para)
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""
"A <menuchoice><guimenu>Szöveg</guimenu><guimenuitem>Unicode-karakterek…</"
"guimenuitem></menuchoice> párbeszédablak használható a betűkészletjelek "
"keresésére és dokumentumba való beillesztésére."

#: tutorial-tips.xml:261(title)
msgid "Using the grid for drawing icons"
msgstr "Ikonok rajzolása rács segítségével"

#: tutorial-tips.xml:262(para)
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"Tegyük fel, hogy rajzolni akar egy 24×24 pixeles ikont. Állítsa a vászon "
"méretét 24×24 pixelre (a <guimenuitem>Dokumentum-beállítások</guimenuitem> "
"ablakban), a rácstávolság pedig legyen 0,5 px (48×48 rácsvonal). Ha ezután a "
"kitöltött objektumokat a <emphasis>páros</emphasis>, a körvonalas "
"objektumokat pedig a <emphasis>páratlan</emphasis> rácsvonalakhoz igazítja, "
"és a körvonalak szélessége pixelben megadva páros szám, továbbá a rajzot az "
"alapértelmezett 96 DPI felbontással (úgy, hogy 1 px megfelel 1 bitkép-"
"pixelnek) exportálja, akkor egy éles bitképet kap, nincs szükség élsimításra."

#: tutorial-tips.xml:273(title)
msgid "Object rotation"
msgstr "Objektumforgatás"

#: tutorial-tips.xml:274(para)
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton role=\"click"
"\">click</mousebutton> again on the object to see the rotation and skew "
"arrows. If the arrows at the corners are clicked and dragged, the object "
"will rotate around the center (shown as a cross mark). If you hold down the "
"<keycap function=\"shift\">Shift</keycap> key while doing this, the rotation "
"will occur around the opposite corner. You can also drag the rotation center "
"to any place."
msgstr ""
"A Kijelölő eszközzel egy objektumra <mousebutton role=\"click\">kattintva</"
"mousebutton> láthatja a méretező nyilakat, ismételt <mousebutton role=\"click"
"\">kattintásra</mousebutton> a forgató és nyíró nyilak jelennek meg. A "
"sarkokban levő valamelyik nyíl húzásával forgathatja az objektumot a "
"középpontja (egy kereszt alakú jel) körül. Ha forgatás közben nyomva tartja "
"a <keycap function=\"shift\">Shift</keycap> billentyűt, akkor a forgatás "
"középpontja átkerül az átellenes sarokba. A forgatás középpontja egérrel is "
"elhúzható bárhová."

#: tutorial-tips.xml:283(para)
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> and <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> (by 90 degrees). The "
"same <keycap>[</keycap><keycap>]</keycap> keys with <keycap function=\"alt"
"\">Alt</keycap> perform slow pixel-size rotation."
msgstr ""
"Forgathat (15°-onként) a <keycap>[</keycap> és a <keycap>]</keycap> "
"billentyűkkel, vagy (90°-onként) a <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> és <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> billentyűkkel is. Ha "
"az <keycap function=\"alt\">Alt</keycap> módosítóval használja a <keycap>[</"
"keycap> vagy <keycap>]</keycap> billentyűk valamelyikét, akkor lassan, "
"pixelenként forgathat."

#: tutorial-tips.xml:292(title)
msgid "Drop shadows"
msgstr "Vetett árnyék"

#: tutorial-tips.xml:293(para)
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""
"Vetett árnyék gyorsan létrehozható bármely objektum számára a "
"<menuchoice><guimenu>Szűrők</guimenu><guisubmenu>Árnyékok és ragyogások</"
"guisubmenu><guimenuitem>Vetett árnyék…</guimenuitem></menuchoice> funkcióval."

#: tutorial-tips.xml:297(para)
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""
"Egyszerűen készíthet egy objektumnak vetett árnyékot elmosással a kitöltés "
"és körvonal palettán. Jelöljön ki egy objektumot, készítsen róla egy "
"másolatot a <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</"
"keycap></keycombo> billentyűkkel, majd a <keycap>PgDown</keycap> "
"lenyomásával helyezze az eredeti objektum mögé a másolatot. Kicsit mozgassa "
"jobbra és lefelé az eredetihez képest, végül a Kitöltés és körvonal "
"párbeszédablakban állítsa az Elmosás értéket mondjuk 5,0-re. Készen is van!"

#: tutorial-tips.xml:307(title)
msgid "Placing text on a path"
msgstr "Szöveg útvonalra illesztése"

#: tutorial-tips.xml:308(para)
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""
"Egy szöveg útvonalra illesztéséhez a szöveget és a görbét is jelölje ki, "
"majd a <guimenu>Szöveg</guimenu> menüből válassza az <guimenuitem>Útvonalra "
"való illesztés</guimenuitem> parancsot. A szöveg az útvonal elején fog "
"kezdődni. Ajánlott egy külön erre a célra készült útvonalra illeszteni, és "
"nem valamelyik másik rajzelemre – ez több módosítási lehetőséget biztosít "
"anélkül, hogy a már kész elemeket kellene bolygatni."

#: tutorial-tips.xml:318(title)
msgid "Selecting the original"
msgstr "Az eredeti kijelölése"

#: tutorial-tips.xml:319(para)
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""
"Ha van egy útvonalra illesztett szövege, egy kapcsolt pereme vagy egy "
"klónja, nehézkes lehet kijelölni a forrásobjektumot vagy ‑útvonalat, mert "
"alul van, láthatatlan vagy zárolt. De a mágikus <keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>D</keycap></keycombo> billentyűparancs a "
"segítségére siet; jelölje ki a szöveget, kapcsolt peremet vagy klónt, és a "
"<keycombo><keycap function=\"shift\">Shift</keycap><keycap>D</keycap></"
"keycombo> hatására a kijelölés áttevődik a megfelelő útvonalra, "
"peremforrásra vagy a klón eredetijére."

#: tutorial-tips.xml:329(title)
msgid "Window off-screen recovery"
msgstr "Az ablakgeometria helyreállítása"

#: tutorial-tips.xml:330(para)
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""
"Ha egy dokumentumot több – a monitorok felbontásában vagy a megjelenítők "
"számában eltérő – rendszeren is megnyit, előfordulhat, hogy az ablak kilóg a "
"képernyőről, mivel az Inkscape az elmentett ablakpozíciót veszi alapul "
"megnyitáskor. Elég maximalizálni az ablakot (miáltal illeszkedni fog a "
"képernyőhöz, használja a tálcaikont), majd mentse el, és nyissa meg újra. Az "
"ablakgeometria mentése ki is kapcsolható (az <guimenuitem>Inkscape "
"Beállítások</guimenuitem> <menuchoice><guimenu>Felület</"
"guimenu><guimenuitem>Ablakok lapján</guimenuitem></menuchoice>)."

#: tutorial-tips.xml:341(title)
msgid "Transparency, gradients, and PostScript export"
msgstr "Átlátszóság, színátmenetek és PostScript-exportálás"

#: tutorial-tips.xml:342(para)
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>d</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"Az <emphasis>átlátszóság</emphasis>ot a PostScript és az EPS formátum nem "
"támogatja, így célszerű kerülni a használatát, ha ilyen formátumokba készül "
"exportálni. Ha egyenletesen átlátszó objektum takar egyszínű objektumot, "
"akkor a következőt teheti: jelölje ki az átlátszó objektumot; váltson a "
"Pipetta eszközre (<keycap>F7</keycap> vagy <keycap>d</keycap>); győződjön "
"meg arról, hogy az eszköz a <guilabel>Átlátszatlanság: Leolvasás</guilabel> "
"gombja deaktiválva van az eszköztáron; kattintson ugyanarra az objektumra. "
"Ezáltal a pipetta felveszi és egyben be is állítja a látható színt, de "
"ezúttal már átlátszóság nélkül. Ismételje ezt minden átlátszó objektumra. Ha "
"az átlátszó objektum különféle egyszínű objektumokat takar, akkor "
"megfelelően fel kell darabolni, és a műveletet ezeken a darabokon kell "
"végrehajtani. Ne feledje, hogy a színpipetta nem változtatja meg az objektum "
"átlátszatlanság értékét, csak a kitöltés vagy a körvonal színének alfa "
"értékét, ezért győződjön meg róla, hogy minden objektum átlátszatlansága "
"100% -ra van állítva, mielőtt elindulna."

#: tutorial-tips.xml:359(title)
msgid "Interactivity"
msgstr ""

#: tutorial-tips.xml:360(para)
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""

#: tutorial-tips.xml:361(para)
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""

#: tutorial-tips.xml:362(para)
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap function=\"shift\">Shift</keycap><keycap>O</keycap></"
"keycombo>). Here you can implement arbitrary functionality using JavaScript. "
"Some basic examples:"
msgstr ""

#: tutorial-tips.xml:365(para)
msgid "Open another file in the current window when clicking on the object:"
msgstr ""

#: tutorial-tips.xml:367(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""

#: tutorial-tips.xml:371(para)
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr ""

#: tutorial-tips.xml:373(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape.org"
"\",\"_blank\");</code>"
msgstr ""

#: tutorial-tips.xml:377(para)
msgid "Reduce transparency of the object while hovering:"
msgstr ""

#: tutorial-tips.xml:379(para)
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""

#: tutorial-tips.xml:380(para)
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tips.xml:0(None)
msgid "translator-credits"
msgstr ""
"Somogyvári Róbert <cr04ch at gmail>, 2008.\n"
"Gyuris Gellért <bubu at ujevangelizacio.hu> 2018, 2019"
