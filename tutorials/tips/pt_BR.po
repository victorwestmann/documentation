msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tips and Tricks\n"
"POT-Creation-Date: 2019-12-17 00:51+0100\n"
"PO-Revision-Date: 2019-12-19 22:56-0800\n"
"Last-Translator: Victor Westmann <victor.westmann@gmail.com>\n"
"Language-Team: Brazilian Portuguese <pt_BR@li.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"

#: tips-f06.svg:136(format) tips-f05.svg:558(format) tips-f04.svg:70(format)
#: tips-f03.svg:49(format) tips-f02.svg:49(format) tips-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:40(None)
msgid "@@image: 'tips-f01.svg'; md5=fa1085ee601bf2189d95bfe01fefbf93"
msgstr "@@image: 'tips-f01.svg';"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:58(None)
msgid "@@image: 'tips-f02.svg'; md5=e96ce4666af44ed3b0deca1da8488fd7"
msgstr "@@image: 'tips-f02.svg';"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:74(None)
msgid "@@image: 'tips-f03.svg'; md5=bfa9ec74c11c4b1511de4ccf5866fbbf"
msgstr "@@image: 'tips-f03.svg';"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:122(None)
msgid "@@image: 'tips-f04.svg'; md5=5c05a79711fab8360381f1777d219078"
msgstr "@@image: 'tips-f04.svg';"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:134(None)
msgid "@@image: 'tips-f05.svg'; md5=815ae6958fe963c2226737ee66426606"
msgstr "@@image: 'tips-f05.svg';"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tips.xml:151(None)
msgid "@@image: 'tips-f06.svg'; md5=e9d4cd268c254a44e1289678ace9cc9e"
msgstr "@@image: 'tips-f06.svg';"

#: tutorial-tips.xml:6(title)
msgid "Tips and Tricks"
msgstr "Dicas e Truques"

#: tutorial-tips.xml:9(para)
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"Este tutorial demonstrará várias dicas e truques que usuários aprenderam com "
"o uso do Inkscape e algumas características “escondidas” que podem te ajudar "
"a acelerar tarefas de produção."

#: tutorial-tips.xml:17(title)
msgid "Radial placement with Tiled Clones"
msgstr "Arranjo radial com Ladrilhar Clones"

#: tutorial-tips.xml:18(para)
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""
"É fácil observar como usar a caixa de diálogos <guimenuitem>Criar Clones "
"Ladrilhados</guimenuitem> para grades retangulares e padrões. Mas e se você "
"precisar de um arranjo <firstterm>radial</firstterm>, onde os objetos "
"compartilham um centro comum de rotação? Isto também é possível!"

#: tutorial-tips.xml:23(para)
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"Se seu padrão radial requer apenas 3, 4, 6, 8 ou 12 elementos, então você "
"pode tentar as simetrias P3, P31M,P3M1, P4, P4M, P6 ou P6M. Estes padrões "
"funcionam perfeitamente para flocos de neve e afins. Entretanto, o próximo "
"método é mais abrangente."

#: tutorial-tips.xml:28(para)
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""
"Escolha a simetria P1 (transição simples) e depois <emphasis>compense</"
"emphasis> essa transição configurando na aba <guimenuitem>Deslocamento</"
"guimenuitem>, <guilabel>Por linha/Deslocar Y</guilabel> e <guilabel>Por "
"coluna/Deslocar X</guilabel> ambos para -100%. Agora todos os clones ficarão "
"empilhados exatamente em cima do original. Tudo o que resta a fazer é ir "
"para a aba <guimenuitem>Rotação</guimenuitem> e configurar algum ângulo de "
"rotação por coluna, e então criar o padrão com uma linha e múltiplas "
"colunas. Por exemplo, aqui está um padrão feito a partir de uma linha "
"horizontal, com 30 colunas, cada uma girada 6 graus:"

#: tutorial-tips.xml:45(para)
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"clones, unlink them first)."
msgstr ""
"Para obter um mostrador de relógio a partir deste, tudo o que você precisa "
"fazer é cortar ou simplesmente cobrir a parte central com um círculo branco "
"(para fazer operações booleanas nos clones, desagrupe-os primeiro)."

#: tutorial-tips.xml:49(para)
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""
"Efeitos mais interessantes podem ser criados usando tanto linhas quanto "
"colunas. Aqui está um padrão com 10 colunas e 8 linhas, com rotação de 2 "
"graus por linha e 18 graus por coluna. Aqui cada grupo de linhas é uma "
"\"coluna\", assim os grupos estão a 18 graus um do outro, dentro de cada "
"coluna, e as linhas individuais, 2 graus afastadas:"

#: tutorial-tips.xml:63(para)
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""
"Nos exemplos acima, a linha foi girada em volta de seu centro. Mas e se você "
"quiser que o centro fique fora da sua forma? Apenas clique no objeto duas "
"vezes com a ferramenta Seletor para entrar no modo de rotação.\n"
"invisível (sem preenchimento e sem traço) que cubra sua forma e cujo centro "
"esteja no ponto que você deseja, agrupe a forma e o retângulo, e então use o "
"<guimenuitem>Ladrilhar Clones</guimenuitem> no objeto. Assim você pode fazer "
"“explosões” ou “explosões de estrelas” fazendo alterações no "

#: tutorial-tips.xml:81(title)
msgid "How to do slicing (multiple rectangular export areas)?"
msgstr "Como fazer fatias (várias áreas retangulares exportadas)?"

#: tutorial-tips.xml:82(para)
msgid ""
"Create a new layer, in that layer create invisible rectangles covering parts "
"of your image. Make sure your document uses the px unit (default), turn on "
"grid and snap the rects to the grid so that each one spans a whole number of "
"px units. Assign meaningful ids to the rects, and export each one to its own "
"file (<menuchoice><guimenu>File</guimenu><guimenuitem>Export PNG Image</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</keycap></"
"keycombo>)). Then the rects will remember their export filenames. After "
"that, it's very easy to re-export some of the rects: switch to the export "
"layer, use <keycap>Tab</keycap> to select the one you need (or use Find by "
"id), and click <guibutton>Export</guibutton> in the dialog. Or, you can "
"write a shell script or batch file to export all of your areas, with a "
"command like:"
msgstr ""
"Crie uma camada nova, nesta camada crie retângulos invisíveis cobrindo "
"partes de sua imagem. Certifique-se de que seu documento use a unidade px "
"(padrão), ative a grade e ajuste os retângulos à grade de modo que cada um "
"abranja um número inteiro de unidades de pixel. Atribua identificações "
"significativas para os retângulos, e exporte cada um em seu próprio arquivo "
"(<menuchoice><guimenu>Arquivo</guimenu><guimenuitem>Exportar Imagens PNG</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</keycap></"
"keycombo>)). Assim os retângulos gravarão seus nomes de exportação. Depois "
"disso, é muito fácil reexportar alguns retângulos: mude para a camada de "
"exportação, use <keycap>Tab</keycap> para selecionar aquele que deseja (ou "
"use Localizar por id), e clique em <guibutton>Exportar</guibutton> na caixa "
"de diálogos. Você pode escrever um shell script ou um arquivo batch para "
"exportar todas as suas áreas, com um comando como:"

#: tutorial-tips.xml:94(command)
msgid "inkscape -i area-id -t filename.svg"
msgstr "inkscape -i area-id -t filename.svg"

#: tutorial-tips.xml:96(para)
msgid ""
"for each exported area. The <command>-t</command> switch tells it to use the "
"remembered filename hint, otherwise you can provide the export filename with "
"the <command>-e</command> switch. Alternatively, you can use the "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Web</"
"guisubmenu><guimenuitem>Slicer</guimenuitem></menuchoice> extensions, or "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Export</"
"guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice> for similar "
"results."
msgstr ""
"para cada área exportada. A opção <command>-t</command> diz para usar a "
"sugestão do nome do arquivo gravada, senão, você pode fornecer o nome do "
"arquivo de exportação com a opção <command>-e</command>. De outro modo, você "
"pode usar a extensão <menuchoice><guimenu>Extensões</"
"guimenu><guisubmenu>Web</guisubmenu><guimenuitem>Slicer</guimenuitem></"
"menuchoice>, ou <menuchoice><guimenu>Extensões</"
"guimenu><guisubmenu>Exportar</guisubmenu><guimenuitem>Guilhotina</"
"guimenuitem></menuchoice> para resultados similares."

#: tutorial-tips.xml:104(title)
msgid "Non-linear gradients"
msgstr "Gradientes não-lineares"

#: tutorial-tips.xml:105(para)
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"A versão 1.1 do SVG não suporta gradientes não-lineares (ou seja, aqueles "
"que tem uma transição não-linear entre as cores). Você pode, entretanto, "
"imitá-los através de gradientes com <firstterm>várias paradas</firstterm>."

#: tutorial-tips.xml:110(para)
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""
"Comece com um simples gradiente de duas paradas (você pode escolher isso na "
"caixa de diálogo de Preenchimento e Contorno ou usar a ferramenta "
"gradiente). Agora, com a ferramenta gradiente, adiciona uma nova parada de "
"gradiente no meio; ou clicando duas vezes na linha do gradiente, ou ao "
"selecionar a parade de gradiente com formato de quadrado e clicando no botão "
"<guimenuitem>Inserindo nova parada</guimenuitem>na barra de ferramenta no "
"topo da ferramenta de gradiente. Arraste a nova parada um pouco. Então "
"adicione mais paradas antes e depois da parada do meio e arraste ela também, "
"para que o gradiente tenha uma aparência suave. Quanto mais paradas você "
"adicionar, mais suave ficará o gradiente resultante. Aqui está o gradiente "
"inicial preto-e-branco com duas paradas:"

#: tutorial-tips.xml:127(para)
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"E aqui, vários gradientes não-lineares com múltiplas paradas (verifique-as "
"no Editor de Gradiente):"

#: tutorial-tips.xml:141(title)
msgid "Excentric radial gradients"
msgstr "Gradientes radiais excêntricos"

#: tutorial-tips.xml:142(para)
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap function=\"shift"
"\">Shift</keycap>. This will move the x-shaped <firstterm>focus handle</"
"firstterm> of the gradient away from its center. When you don't need it, you "
"can snap the focus back by dragging it close to the center."
msgstr ""
"Gradientes radiais não têm que ser simétricos. Com a ferramenta Gradiente "
"arraste a alça central de um gradiente elíptico com <keycap function=\"shift"
"\">Shift</keycap>. Isto fará mover a <firstterm>alça de foco</firstterm> em "
"forma de x do gradiente para longe do seu centro. Quando você não precisar "
"mais dele, você pode ajustar o foco de volta à sua posição arrastando-o para "
"perto do centro."

#: tutorial-tips.xml:158(title)
msgid "Aligning to the center of the page"
msgstr "Alinhando ao centro da página"

#: tutorial-tips.xml:159(para)
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""
"Para alinhar alguma coisa ao centro ou ao lado de uma página, selecione o "
"objeto ou grupo e depois escolha uma opção em <guimenuitem>Página</"
"guimenuitem> dentro da lista em <guilabel>Relativo a:</guilabel> na caixa de "
"diálogos <guimenuitem>Alinhar e Distribuir</guimenuitem>(<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>A</keycap></keycombo>)."

#: tutorial-tips.xml:167(title)
msgid "Cleaning up the document"
msgstr "Limpando o documento"

#: tutorial-tips.xml:168(para)
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""
"Muitos dos gradientes não mais usados, padrões, e marcadores (mais "
"precisamente, aqueles que você editou manualmente) permanecem nas paletas "
"correspondentes e podem ser usados novamente para novos objetos. Entretanto, "
"se você quiser otimizar seu documento, use o comando <guimenuitem>Limpar "
"Documento</guimenuitem> no menu <guimenu>Arquivo</guimenu>. Isso removerá "
"quaisquer gradientes, padrões, ou marcadores que não estejam sendo sendo "
"usados no documento, deixando o arquivo menor."

#: tutorial-tips.xml:177(title)
msgid "Hidden features and the XML editor"
msgstr "Características escondidas e o editor XML"

#: tutorial-tips.xml:178(para)
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""
"O editor XML (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) te permite "
"alterar quase todos os aspectos do documento sem ter que usar um editor de "
"textos externo. Além disso, o Inkscape também costuma ser compatível com "
"mais recursos SVG dos que os que podem ser acessados pela IGU. O editor XML "
"é uma das formas de ter acesso a estes recursos (se você sabe SVG)."

#: tutorial-tips.xml:187(title)
msgid "Changing the rulers' unit of measure"
msgstr "Modificando a unidade de medida das réguas"

#: tutorial-tips.xml:188(para)
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-left corner and "
"preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Page</"
"guimenuitem> tab."
msgstr ""
"No modelo padrão, a unidade de medida usada pelas réguas está em mm. Esta é "
"também a unidade usada para mostrar as coordenadas no canto inferior "
"esquerdo e pré-selecionado em todos os menus de unidade. (Você pode sempre "
"pairar seu <mousebutton>mouse</mousebutton> sobre uma régua para ver a dica "
"de qual unidade a régua usa.) Para mudra isso, abra<guimenuitem>Propriedades "
"do Documento</guimenuitem> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>) e mude a <guimenuitem>Mostrar unidades</guimenuitem> na "
"aba<guimenuitem>Página</guimenuitem>."

#: tutorial-tips.xml:199(title)
msgid "Stamping"
msgstr "Estampagem"

#: tutorial-tips.xml:200(para)
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""
"Para criar rapidamente várias cópias de um objeto, use a "
"<firstterm>estampagem</firstterm>. Simplesmente arraste um objeto (ou amplie-"
"o ou rotacione-o), e enquanto mantém pressionado o botão do <mousebutton "
"role=\"click\">mouse</mousebutton>, pressione <keycap>Barra de Espaço</"
"keycap>. Isto cria uma “estampa” do objeto selecionado. Você pode repeti-lo "
"quantas vezes desejar."

#: tutorial-tips.xml:209(title)
msgid "Pen tool tricks"
msgstr "Truques da ferramenta Bezier"

#: tutorial-tips.xml:210(para)
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""
"Na ferramenta Caneta (Bezier), você tem as seguintes opções para finalizar a "
"linha:"

#: tutorial-tips.xml:214(para)
msgid "Press <keycap>Enter</keycap>"
msgstr "Pressionar <keycap>Enter</keycap>"

#: tutorial-tips.xml:217(para)
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr ""
"<mousebutton role=\"double-click\">Duplo clique</mousebutton> com o botão "
"esquerdo do mouse"

#: tutorial-tips.xml:220(para)
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""
"Clicando com o botão <mousebutton role=\"right-click\">direito</mousebutton> "
"do mouse"

#: tutorial-tips.xml:223(para)
msgid "Select another tool"
msgstr "Selecionar outra ferramenta"

#: tutorial-tips.xml:228(para)
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""
"Observe que enquanto o caminho não estiver finalizado (i.e. for exibido em "
"verde, com o segmento atual em vermelho) ele ainda não existe como um objeto "
"no documento. Dessa maneira, para cancelá-lo, use tanto <keycap>Esc</keycap> "
"(cancela o caminho inteiro) quanto <keycap>Backspace</keycap> (remove o "
"último segmento do caminho não finalizado) em vez do de "
"<guimenuitem>Desfazer</guimenuitem>."

#: tutorial-tips.xml:235(para)
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""
"Para adicionar um novo subcaminho a um caminho existente, selecione o "
"caminho e comece a desenhar com <keycap function=\"shift\">Shift</keycap> "
"Entretanto, se o que você quer é simplesmente <emphasis>continuar</emphasis> "
"um caminho existente, Shift não é necessário; apenas comece a desenhar a "
"partir de um dos nós finais do caminho selecionado."

#: tutorial-tips.xml:244(title)
msgid "Entering Unicode values"
msgstr "Inserindo valores Unicode"

#: tutorial-tips.xml:245(para)
msgid ""
"While in the Text tool, pressing <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles between Unicode and "
"normal mode. In Unicode mode, each group of 4 hexadecimal digits you type "
"becomes a single Unicode character, thus allowing you to enter arbitrary "
"symbols (as long as you know their Unicode codepoints and the font supports "
"them). To finish the Unicode input, press <keycap>Enter</keycap>. For "
"example, <keycombo action=\"seq\"><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</"
"keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo> inserts an em-"
"dash (—). To quit the Unicode mode without inserting anything press "
"<keycap>Esc</keycap>."
msgstr ""
"Enquanto você estiver na ferramenta Texto, pressione <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>U</keycap></keycombo> que isso "
"alternará entre o modo Unicode e o modo normal. No modo Unicode, cada grupo "
"de 4 dígitos hexadecimais que você digita se torna um único caracter "
"Unicode, permitindo a você entrar com símbolos arbitrários (contanto que "
"você saiba o seu código Unicode e quais fontes são com patíveis com eles). "
"Para concluir a entrada Unicode, pressione <keycap>Enter</keycap>. Por "
"exemplo, <keycombo action=\"seq\"><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</"
"keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo> insere um "
"travessão (—). Para sair do modo Unicode sem inserir nada pressione "
"<keycap>Esc</keycap>."

#: tutorial-tips.xml:254(para)
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""
"Você também pode usar a caixa de diálogo <menuchoice><guimenu>Texto</"
"guimenu><guimenuitem>Caracteres Unicode</guimenuitem></menuchoice> para "
"procurar por e inserir glifos em seu documento."

#: tutorial-tips.xml:261(title)
msgid "Using the grid for drawing icons"
msgstr "Usando a grade para desenhar ícones"

#: tutorial-tips.xml:262(para)
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"Suponha que você queira criar um ícone de 24x24 pixels de tamanho. Crie uma "
"tela de 24x24 px (use <guimenuitem>Preferências do Documento</guimenuitem>) "
"e configure a grade para 0,5 px (linhas de grade de 48x48). Agora, se você "
"alinhar objetos preenchidos a linhas <emphasis>pares</emphasis> da grade, e "
"objetos de contorno a linhas de grade <emphasis>ímpares</emphasis> com a "
"espessura do traço em px um número par, e exportá-lo no valor padrão 96dpi "
"(de modo que 1 px se transforme em 1 pixel de bitmap), você obterá uma "
"imagem bitmap clara, sem necessidade de suavização."

#: tutorial-tips.xml:273(title)
msgid "Object rotation"
msgstr "Rotação de objetos"

#: tutorial-tips.xml:274(para)
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton role=\"click"
"\">click</mousebutton> again on the object to see the rotation and skew "
"arrows. If the arrows at the corners are clicked and dragged, the object "
"will rotate around the center (shown as a cross mark). If you hold down the "
"<keycap function=\"shift\">Shift</keycap> key while doing this, the rotation "
"will occur around the opposite corner. You can also drag the rotation center "
"to any place."
msgstr ""
"Com a ferramenta Seleção, <mousebutton role=\"click\">clique</mousebutton> "
"em um objeto para ver as setas de dimensionamento, depois <mousebutton role="
"\"click\">clique</mousebutton> novamente no objeto para ver as setas de "
"rotação e inclinação. Se clicar nas setas do canto e arrastá-las, o objeto "
"será girado em volta do centro (exibido como uma marca de cruz). Se você "
"manter pressionada a tecla <keycap function=\"shift\">Shift</keycap>enquanto "
"faz isso, a rotação ocorrerá ao redor do canto oposto. Você pode também "
"arrastar o centro de rotação para qualquer lugar."

#: tutorial-tips.xml:283(para)
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> and <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> (by 90 degrees). The "
"same <keycap>[</keycap><keycap>]</keycap> keys with <keycap function=\"alt"
"\">Alt</keycap> perform slow pixel-size rotation."
msgstr ""
"Ou, você pode rodar pelo teclado ao pressionar <keycap>[</keycap> e "
"<keycap>]</keycap> (por 15 graus) ou <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> e <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> (por 90 graus). As "
"mesmas <keycap>[</keycap><keycap>]</keycap> teclas com <keycap function=\"alt"
"\">Alt</keycap> realizam uma rotação lenta em tamanho-de-pixel."

#: tutorial-tips.xml:292(title)
msgid "Drop shadows"
msgstr "Sombras de fundo em bitmaps"

#: tutorial-tips.xml:293(para)
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""
"Para rapidamente criar sombras para objetos, use a opção de menu do "
"recurso<menuchoice><guimenu>Filtros</guimenu><guisubmenu>Sombras e Brilhos</"
"guisubmenu><guimenuitem>Sombra</guimenuitem></menuchoice>."

#: tutorial-tips.xml:297(para)
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""
"Você pode facilmente criar sombras borradas manualmente para objetos com "
"borrão na caixa de diálogo Preenchimento e Contorno. Selecione um objeto, "
"duplique-o <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</"
"keycap></keycombo>, pressione <keycap>PgDown</keycap> para colocá-lo embaixo "
"do objeto original, coloque um pouco para a direita e abaixo do objeto "
"original. Agora abra a caixa de diálogo Preenchimento e Contorno e mude o "
"valor do borrão para, digamos, 5.0. É isso!"

#: tutorial-tips.xml:307(title)
msgid "Placing text on a path"
msgstr "Posicionando texto em um caminho"

#: tutorial-tips.xml:308(para)
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""
"Para colocar texto ao longo de uma curva, selecione o texto e a curva juntos "
"e escolha <guimenuitem>Pôr no caminho</guimenuitem> do menu <guimenu>Texto</"
"guimenu>. O texto começará no início do caminho. No geral, é melhor criar um "
"caminho explícito sobre o qual você queira ajustar o texto, em vez de ajustá-"
"lo em algum outro elemento de desenho – isto te dará mais controle sem ter "
"que deslocar seu desenho."

#: tutorial-tips.xml:318(title)
msgid "Selecting the original"
msgstr "Selecionando o original"

#: tutorial-tips.xml:319(para)
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""
"Quando você tem um texto em um caminho, um link offset, ou um clone, pode "
"ser difícil de seus objetos/caminhos fontes serem selecionados porque podem "
"estar diretamente na camada de baixo sob outros objetos, invisíveis e/ou "
"bloqueados. As teclas mágicas <keycap>Shift+D</keycap> te ajudarão; "
"selecione o texto, offset, ou clone, e pressione <keycap>Shift+D</keycap> "
"para mover a seleção entre o caminho, offset, ou o clone original "
"correspondente."

#: tutorial-tips.xml:329(title)
msgid "Window off-screen recovery"
msgstr "Recuperação de janelas fora da tela"

#: tutorial-tips.xml:330(para)
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""
"Quando transferir documentos entre sistemas com diferentes resoluções ou "
"número de telas, você pode perceber que o Inkscape salvou a posição de uma "
"janela que a coloca fora de alcance na sua tela. Simplesmente maximize a "
"janela (que vai trazer ela de volta à sua visão, use a barra de tarefas), "
"salve e recarregue. Você pode evitar isto por complete desmarcando a opção "
"global Salvar a posição das janelas (Seção <guimenuitem>Preferências do "
"Inkscape</guimenuitem>, <menuchoice><guimenu>Interface</"
"guimenu><guimenuitem>Janelas</guimenuitem></menuchoice>)."

#: tutorial-tips.xml:341(title)
msgid "Transparency, gradients, and PostScript export"
msgstr "Exportação de transparência, gradientes e PostScript"

#: tutorial-tips.xml:342(para)
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>d</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"Os formatos PostScript ou EPS não são compatíveis com "
"<emphasis>transparência</emphasis>, então você não deve usá-los se for "
"exportá-los para PS/EPS. No caso de transparência que sobrepõe a cor lisa, é "
"fácil consertar isso: Selecione um dos objetos transparentes; mude para a "
"ferramenta Conta-gotas (<keycap>F7</keycap> ou <keycap>d</keycap>); "
"certifique-se que o botão <guilabel>Opacidade: Escolher</guilabel> na barra "
"de ferramentas da ferramenta Conta gotas está desativado; clique no mesmo "
"objeto. Isto vai pegar a cor visível e atribuí-la de volta ao objeto, mas "
"desta vez sem transparência. Repita para todos os objetos transparentes. Se "
"seu objeto transparente sobrepor várias áreas de cor lisa, você vai ter que "
"quebrá-lo em pedaços e aplicar este procedimento para cada pedaço."

#: tutorial-tips.xml:359(title)
msgid "Interactivity"
msgstr "Interatividade"

#: tutorial-tips.xml:360(para)
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""
"A maioria dos elementos SVG podem ser editados para reagir à entrada do "
"usuário (geralmente isso só vai funcionar se o SVG for exibido em um "
"navegador)."

#: tutorial-tips.xml:361(para)
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""
"A possibilidade mais simples é de adicionar um link clicável a objetos. Para "
"isso clique com o <mousebutton role=\"right-click\">botão direito do mouse</"
"mousebutton> no objeto e selecione <menuchoice><guimenuitem>Criar Link</"
"guimenuitem></menuchoice> do menu de contexto. A caixa de diálogo "
"\"Atributos do objeto\" vai se abrir, lá você poderá definir o link de "
"destino usando o valor <guilabel>href</guilabel>."

#: tutorial-tips.xml:362(para)
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap function=\"shift\">Shift</keycap><keycap>O</keycap></"
"keycombo>). Here you can implement arbitrary functionality using JavaScript. "
"Some basic examples:"
msgstr ""
"Mais controle é possível usando os atributos de interatividade acessíveis a "
"partir da caixa de diálogo \"Propriedades do Objeto\" (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap function=\"shift\">Shift</"
"keycap><keycap>O</keycap></keycombo>). Aqui você pode implementar "
"funcionalidades arbitrárias usando JavaScript. Alguns exemplos básicos:"

#: tutorial-tips.xml:365(para)
msgid "Open another file in the current window when clicking on the object:"
msgstr "Abra outro arquivo na janela atual quando clicar no objeto:"

#: tutorial-tips.xml:367(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""
"Defina <guilabel>onclick</guilabel> para <code>window.location='file2.svg';</"
"code>"

#: tutorial-tips.xml:371(para)
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr "Abra um link qualquer da web em uma nova janela ao clicar no objeto:"

#: tutorial-tips.xml:373(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape.org"
"\",\"_blank\");</code>"
msgstr ""
"Defina <guilabel>onclick</guilabel> em <code>window.open(\"https://inkscape."
"org\",\"_blank\");</code>"

#: tutorial-tips.xml:377(para)
msgid "Reduce transparency of the object while hovering:"
msgstr "Reduzir transparência do objeto enquanto estiver pairando:"

#: tutorial-tips.xml:379(para)
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""
"Defina <guilabel>onmouseover</guilabel> em <code>style.opacity = 0.5;</code>"

#: tutorial-tips.xml:380(para)
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""
"Defina <guilabel>onmouseout</guilabel> em <code>style.opacity = 1;</code>"

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tips.xml:0(None)
msgid "translator-credits"
msgstr ""
"Thiago Pimentel <thiago.merces@gmail.com>, 2006,\n"
"Victor Westmann <victor.westmann@gmail.com>, 2019."
