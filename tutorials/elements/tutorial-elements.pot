msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-12-17 00:51+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: elements-f15.svg:452(format) elements-f14.svg:173(format) elements-f13.svg:102(format) elements-f12.svg:445(format) elements-f11.svg:48(format) elements-f10.svg:48(format) elements-f09.svg:48(format) elements-f08.svg:206(format) elements-f07.svg:91(format) elements-f06.svg:91(format) elements-f05.svg:48(format) elements-f04.svg:48(format) elements-f03.svg:48(format) elements-f02.svg:48(format) elements-f01.svg:48(format)
msgid "image/svg+xml"
msgstr ""

#: elements-f12.svg:710(tspan)
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr ""

#: elements-f12.svg:721(tspan)
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr ""

#: elements-f12.svg:726(tspan)
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr ""

#: elements-f12.svg:731(tspan)
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr ""

#: elements-f04.svg:79(tspan)
#, no-wrap
msgid "BIG"
msgstr ""

#: elements-f04.svg:93(tspan)
#, no-wrap
msgid "small"
msgstr ""

#: elements-f01.svg:88(tspan)
#, no-wrap
msgid "Elements"
msgstr ""

#: elements-f01.svg:101(tspan)
#, no-wrap
msgid "Principles"
msgstr ""

#: elements-f01.svg:114(tspan) tutorial-elements.xml:92(title)
#, no-wrap
msgid "Color"
msgstr ""

#: elements-f01.svg:127(tspan) tutorial-elements.xml:29(title)
#, no-wrap
msgid "Line"
msgstr ""

#: elements-f01.svg:140(tspan) tutorial-elements.xml:45(title)
#, no-wrap
msgid "Shape"
msgstr ""

#: elements-f01.svg:153(tspan) tutorial-elements.xml:75(title)
#, no-wrap
msgid "Space"
msgstr ""

#: elements-f01.svg:166(tspan) tutorial-elements.xml:109(title)
#, no-wrap
msgid "Texture"
msgstr ""

#: elements-f01.svg:179(tspan) tutorial-elements.xml:124(title)
#, no-wrap
msgid "Value"
msgstr ""

#: elements-f01.svg:192(tspan) tutorial-elements.xml:61(title)
#, no-wrap
msgid "Size"
msgstr ""

#: elements-f01.svg:205(tspan) tutorial-elements.xml:146(title)
#, no-wrap
msgid "Balance"
msgstr ""

#: elements-f01.svg:218(tspan) tutorial-elements.xml:162(title)
#, no-wrap
msgid "Contrast"
msgstr ""

#: elements-f01.svg:231(tspan) tutorial-elements.xml:175(title)
#, no-wrap
msgid "Emphasis"
msgstr ""

#: elements-f01.svg:244(tspan) tutorial-elements.xml:190(title)
#, no-wrap
msgid "Proportion"
msgstr ""

#: elements-f01.svg:257(tspan) tutorial-elements.xml:204(title)
#, no-wrap
msgid "Pattern"
msgstr ""

#: elements-f01.svg:271(tspan) tutorial-elements.xml:218(title)
#, no-wrap
msgid "Gradation"
msgstr ""

#: elements-f01.svg:284(tspan) tutorial-elements.xml:236(title)
#, no-wrap
msgid "Composition"
msgstr ""

#: elements-f01.svg:297(tspan)
#, no-wrap
msgid "Overview"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:19(None)
msgid "@@image: 'elements-f01.svg'; md5=5d525322c9f5f91ecda0b29af5f1763b"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:38(None)
msgid "@@image: 'elements-f02.svg'; md5=5ecbf9268a5c65e15d46ae70ce3ed752"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:54(None)
msgid "@@image: 'elements-f03.svg'; md5=f3d269900dd0adf807009ebbd64900ce"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:68(None)
msgid "@@image: 'elements-f04.svg'; md5=ec03f89659797f44d016783b7178622e"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:85(None)
msgid "@@image: 'elements-f05.svg'; md5=21ebd0631018cc955ff7dc15647142c4"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:102(None)
msgid "@@image: 'elements-f06.svg'; md5=a41cd4ea607a0a39b3a644ac15ccd070"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:117(None)
msgid "@@image: 'elements-f07.svg'; md5=fcc89291ba8c1301d1126caec09b19d7"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:133(None)
msgid "@@image: 'elements-f08.svg'; md5=122ae5ad1dc2720fe630e0954ef0cd53"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:155(None)
msgid "@@image: 'elements-f09.svg'; md5=0a75605bc8ad29894786d7a93cc519f0"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:168(None)
msgid "@@image: 'elements-f10.svg'; md5=23b1c69f451d6d8fdb9f21f60f5b3c49"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:183(None)
msgid "@@image: 'elements-f11.svg'; md5=f84abd1fa4d9c2943fcfa81f72d75b66"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:197(None)
msgid "@@image: 'elements-f12.svg'; md5=27a0b1dbcf3b7482369b7f6990fe3dc6"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:211(None)
msgid "@@image: 'elements-f13.svg'; md5=578fdc498ed1760618a2395e24ba8240"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:227(None)
msgid "@@image: 'elements-f14.svg'; md5=9ccc0170b9ab2d852d83baccfe174369"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-elements.xml:240(None)
msgid "@@image: 'elements-f15.svg'; md5=957e9703816c1f5ce0300a2377a28929"
msgstr ""

#: tutorial-elements.xml:6(title)
msgid "Elements of design"
msgstr ""

#: tutorial-elements.xml:9(para)
msgid "This tutorial will demonstrate the elements and principles of design which are normally taught to early art students in order to understand various properties used in art making. This is not an exhaustive list, so please add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""

#: tutorial-elements.xml:25(title)
msgid "Elements of Design"
msgstr ""

#: tutorial-elements.xml:26(para)
msgid "The following elements are the building blocks of design."
msgstr ""

#: tutorial-elements.xml:30(para)
msgid "A line is defined as a mark with length and direction, created by a point that moves across a surface. A line can vary in length, width, direction, curvature, and color. Line can be two-dimensional (a pencil line on paper), or implied three-dimensional."
msgstr ""

#: tutorial-elements.xml:46(para)
msgid "A flat figure, shape is created when actual or implied lines meet to surround a space. A change in color or shading can define a shape. Shapes can be divided into several types: geometric (square, triangle, circle) and organic (irregular in outline)."
msgstr ""

#: tutorial-elements.xml:62(para)
msgid "This refers to variations in the proportions of objects, lines or shapes. There is a variation of sizes in objects either real or imagined."
msgstr ""

#: tutorial-elements.xml:76(para)
msgid "Space is the empty or open area between, around, above, below, or within objects. Shapes and forms are made by the space around and within them. Space is often called three-dimensional or two- dimensional. Positive space is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""

#: tutorial-elements.xml:93(para)
msgid "Color is the perceived character of a surface according to the wavelength of light reflected from it. Color has three dimensions: HUE (another word for color, indicated by its name such as red or yellow), VALUE (its lightness or darkness), INTENSITY (its brightness or dullness)."
msgstr ""

#: tutorial-elements.xml:110(para)
msgid "Texture is the way a surface feels (actual texture) or how it may look (implied texture). Textures are described by word such as rough, silky, or pebbly."
msgstr ""

#: tutorial-elements.xml:125(para)
msgid "Value is how dark or how light something looks. We achieve value changes in color by adding black or white to the color. Chiaroscuro uses value in drawing by dramatically contrasting lights and darks in a composition."
msgstr ""

#: tutorial-elements.xml:142(title)
msgid "Principles of Design"
msgstr ""

#: tutorial-elements.xml:143(para)
msgid "The principles use the elements of design to create a composition."
msgstr ""

#: tutorial-elements.xml:147(para)
msgid "Balance is a feeling of visual equality in shape, form, value, color, etc. Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly balanced. Objects, values, colors, textures, shapes, forms, etc., can be used in creating a balance in a composition."
msgstr ""

#: tutorial-elements.xml:163(para)
msgid "Contrast is the juxtaposition of opposing elements"
msgstr ""

#: tutorial-elements.xml:176(para)
msgid "Emphasis is used to make certain parts of their artwork stand out and grab your attention. The center of interest or focal point is the place a work draws your eye to first."
msgstr ""

#: tutorial-elements.xml:191(para)
msgid "Proportion describes the size, location or amount of one thing compared to another."
msgstr ""

#: tutorial-elements.xml:205(para)
msgid "Pattern is created by repeating an element (line, shape or color) over and over again."
msgstr ""

#: tutorial-elements.xml:219(para)
msgid "Gradation of size and direction produce linear perspective. Gradation of color from warm to cool and tone from dark to light produce aerial perspective. Gradation can add interest and movement to a shape. A gradation from dark to light will cause the eye to move along a shape."
msgstr ""

#: tutorial-elements.xml:237(para)
msgid "The combining of distinct elements to form a whole."
msgstr ""

#: tutorial-elements.xml:247(title)
msgid "Bibliography"
msgstr ""

#: tutorial-elements.xml:248(para)
msgid "This is a partial bibliography used to build this document."
msgstr ""

#: tutorial-elements.xml:251(ulink)
msgid "http://www.makart.com/resources/artclass/EPlist.html"
msgstr ""

#: tutorial-elements.xml:254(ulink)
msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
msgstr ""

#: tutorial-elements.xml:257(ulink)
msgid "http://www.johnlovett.com/test.htm"
msgstr ""

#: tutorial-elements.xml:261(ulink)
msgid "http://digital-web.com/articles/elements_of_design/"
msgstr ""

#: tutorial-elements.xml:265(ulink)
msgid "http://digital-web.com/articles/principles_of_design/"
msgstr ""

#: tutorial-elements.xml:270(para)
msgid "Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. Also, thanks to the Open Clip Art Library (<ulink url=\"http://www.openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics people have submitted to that project."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-elements.xml:0(None)
msgid "translator-credits"
msgstr ""

